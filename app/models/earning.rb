# == Schema Information
#
# Table name: earnings
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  amount         :decimal(10, 2)
#  withdrawn      :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :integer
#
# Indexes
#
#  index_earnings_on_appointment_id  (appointment_id)
#  index_earnings_on_user_id         (user_id)
#
# Foreign Keys
#
#  fk_rails_09a9a2d244  (appointment_id => appointments.id)
#  fk_rails_854d4c9a09  (user_id => users.id)
#

class Earning < ActiveRecord::Base
  belongs_to :user
  belongs_to :appointment

  validates :user_id, :amount, presence: true

  WITHDRAWAL_MIN_LIMIT = 500
end
