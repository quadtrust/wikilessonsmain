class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :school
      t.date :year_from
      t.date :year_to
      t.text :description
      t.belongs_to :profile, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
