class AddUserToRefund < ActiveRecord::Migration
  def change
    add_reference :refunds, :user, index: true, foreign_key: true
  end
end
