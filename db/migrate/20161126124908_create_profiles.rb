class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :firstname
      t.string :lastname
      t.string :avatar
      t.string :phone1
      t.string :phone2
      t.text :about
      t.string :custom_video
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
