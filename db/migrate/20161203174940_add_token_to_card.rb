class AddTokenToCard < ActiveRecord::Migration
  def change
    add_column :cards, :token, :string
    add_index :cards, :token
  end
end
