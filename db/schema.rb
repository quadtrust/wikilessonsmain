# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161222173055) do

  create_table "appointments", force: :cascade do |t|
    t.text     "description",        limit: 65535
    t.boolean  "accepted",                         default: false
    t.integer  "completed",          limit: 4,     default: 0
    t.integer  "student_id",         limit: 4
    t.integer  "teacher_id",         limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "topic",              limit: 255
    t.boolean  "class_valid",                      default: false
    t.datetime "appointment_date"
    t.integer  "class_hours",        limit: 4
    t.integer  "job_id",             limit: 4
    t.integer  "class_status",       limit: 4,     default: 0
    t.boolean  "started_by_student",               default: false
    t.boolean  "started_by_teacher",               default: false
    t.datetime "started_at"
    t.datetime "ended_at"
  end

  add_index "appointments", ["student_id"], name: "index_appointments_on_student_id", using: :btree
  add_index "appointments", ["teacher_id"], name: "index_appointments_on_teacher_id", using: :btree

  create_table "cards", force: :cascade do |t|
    t.string   "brand",       limit: 255
    t.string   "country",     limit: 255
    t.string   "month",       limit: 255
    t.string   "year",        limit: 255
    t.string   "last_digits", limit: 255
    t.boolean  "default"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "user_id",     limit: 4
    t.string   "token",       limit: 255
  end

  add_index "cards", ["token"], name: "index_cards_on_token", using: :btree
  add_index "cards", ["user_id"], name: "index_cards_on_user_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "earnings", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.decimal  "amount",                   precision: 10, scale: 2
    t.boolean  "withdrawn",                                         default: false
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.integer  "appointment_id", limit: 4
  end

  add_index "earnings", ["appointment_id"], name: "index_earnings_on_appointment_id", using: :btree
  add_index "earnings", ["user_id"], name: "index_earnings_on_user_id", using: :btree

  create_table "educations", force: :cascade do |t|
    t.string   "school",      limit: 255
    t.date     "year_from"
    t.date     "year_to"
    t.text     "description", limit: 65535
    t.integer  "profile_id",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "educations", ["profile_id"], name: "index_educations_on_profile_id", using: :btree

  create_table "experiences", force: :cascade do |t|
    t.string   "subject",         limit: 255
    t.text     "description",     limit: 65535
    t.integer  "experience_time", limit: 4
    t.integer  "profile_id",      limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "experiences", ["profile_id"], name: "index_experiences_on_profile_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id",   limit: 4
    t.string  "unsubscriber_type", limit: 255
    t.integer "conversation_id",   limit: 4
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    limit: 255, default: ""
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type",                 limit: 255
    t.text     "body",                 limit: 65535
    t.string   "subject",              limit: 255,   default: ""
    t.integer  "sender_id",            limit: 4
    t.string   "sender_type",          limit: 255
    t.integer  "conversation_id",      limit: 4
    t.boolean  "draft",                              default: false
    t.string   "notification_code",    limit: 255
    t.integer  "notified_object_id",   limit: 4
    t.string   "notified_object_type", limit: 255
    t.string   "attachment",           limit: 255
    t.datetime "updated_at",                                         null: false
    t.datetime "created_at",                                         null: false
    t.boolean  "global",                             default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id",     limit: 4
    t.string   "receiver_type",   limit: 255
    t.integer  "notification_id", limit: 4,                   null: false
    t.boolean  "is_read",                     default: false
    t.boolean  "trashed",                     default: false
    t.boolean  "deleted",                     default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "is_delivered",                default: false
    t.string   "delivery_method", limit: 255
    t.string   "message_id",      limit: 255
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.text     "text",       limit: 65535
    t.string   "link",       limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "student_id",     limit: 4
    t.integer  "teacher_id",     limit: 4
    t.decimal  "teacher_pay",                  precision: 16, scale: 2
    t.decimal  "profit_pay",                   precision: 16, scale: 2
    t.decimal  "total_pay",                    precision: 16, scale: 2
    t.boolean  "paid",                                                  default: false
    t.text     "comment",        limit: 65535
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.integer  "appointment_id", limit: 4
    t.string   "stripeid",       limit: 255
  end

  add_index "payments", ["appointment_id"], name: "index_payments_on_appointment_id", using: :btree
  add_index "payments", ["student_id"], name: "index_payments_on_student_id", using: :btree
  add_index "payments", ["teacher_id"], name: "index_payments_on_teacher_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.string   "firstname",            limit: 255
    t.string   "lastname",             limit: 255
    t.string   "avatar",               limit: 255
    t.string   "phone1",               limit: 255
    t.string   "phone2",               limit: 255
    t.text     "about",                limit: 65535
    t.string   "custom_video",         limit: 255
    t.integer  "user_id",              limit: 4
    t.datetime "created_at",                                                                  null: false
    t.datetime "updated_at",                                                                  null: false
    t.integer  "profile_status",       limit: 4
    t.boolean  "verified",                                                    default: false
    t.decimal  "rate",                               precision: 16, scale: 2
    t.string   "address",              limit: 255
    t.string   "native_language",      limit: 255
    t.string   "additional_languages", limit: 255
    t.integer  "standard",             limit: 4
    t.string   "stripe_customer_id",   limit: 255
    t.float    "average_rating",       limit: 24,                             default: 0.0
    t.integer  "gender",               limit: 4,                              default: 0
    t.integer  "availability",         limit: 4,                              default: 1
    t.string   "zone",                 limit: 255
  end

  add_index "profiles", ["stripe_customer_id"], name: "index_profiles_on_stripe_customer_id", using: :btree
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "refunds", force: :cascade do |t|
    t.boolean  "status"
    t.decimal  "amount",                       precision: 10, scale: 2
    t.text     "comment",        limit: 65535
    t.integer  "appointment_id", limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.integer  "user_id",        limit: 4
  end

  add_index "refunds", ["appointment_id"], name: "index_refunds_on_appointment_id", using: :btree
  add_index "refunds", ["user_id"], name: "index_refunds_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["context"], name: "index_taggings_on_context", using: :btree
  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
  add_index "taggings", ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
  add_index "taggings", ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
  add_index "taggings", ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
  add_index "taggings", ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.integer "taggings_count", limit: 4,   default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "testimonials", force: :cascade do |t|
    t.text     "description",    limit: 65535
    t.float    "rating",         limit: 24
    t.integer  "appointment_id", limit: 4
    t.integer  "user_id",        limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "to_user_id",     limit: 4
  end

  add_index "testimonials", ["appointment_id"], name: "index_testimonials_on_appointment_id", using: :btree
  add_index "testimonials", ["user_id"], name: "index_testimonials_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "username",               limit: 255
    t.integer  "role",                   limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "cards", "users"
  add_foreign_key "earnings", "appointments"
  add_foreign_key "earnings", "users"
  add_foreign_key "educations", "profiles"
  add_foreign_key "experiences", "profiles"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "notifications", "users"
  add_foreign_key "payments", "appointments"
  add_foreign_key "profiles", "users"
  add_foreign_key "refunds", "appointments"
  add_foreign_key "refunds", "users"
  add_foreign_key "testimonials", "appointments"
  add_foreign_key "testimonials", "users"
end
