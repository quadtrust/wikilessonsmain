# == Schema Information
#
# Table name: appointments
#
#  id               :integer          not null, primary key
#  description      :text(65535)
#  accepted         :boolean          default(FALSE)
#  completed        :integer          default(0)
#  student_id       :integer
#  teacher_id       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  topic            :string(255)
#  class_valid      :boolean          default(FALSE)
#  appointment_date :datetime
#  class_hours      :integer
#
# Indexes
#
#  index_appointments_on_student_id  (student_id)
#  index_appointments_on_teacher_id  (teacher_id)
#

require 'rails_helper'

RSpec.describe Appointment, type: :model do
  it { should have_many(:payments) }
  it { should validate_presence_of(:topic) }
  it { should validate_presence_of(:description) }
end
