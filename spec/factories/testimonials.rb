FactoryGirl.define do
  factory :testimonial do
    description "MyText"
    rating 1.5
    appointment nil
    user nil
  end
end
