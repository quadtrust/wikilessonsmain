class AddToUserIdToTestimonial < ActiveRecord::Migration
  def change
    add_column :testimonials, :to_user_id, :integer
  end
end
