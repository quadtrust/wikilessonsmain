class AddStripeidToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :stripeid, :string
  end
end
