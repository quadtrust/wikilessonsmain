class AddAverageRatingToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :average_rating, :float, default: 0
  end
end
