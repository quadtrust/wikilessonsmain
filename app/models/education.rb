# == Schema Information
#
# Table name: educations
#
#  id          :integer          not null, primary key
#  school      :string(255)
#  year_from   :date
#  year_to     :date
#  description :text(65535)
#  profile_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_educations_on_profile_id  (profile_id)
#
# Foreign Keys
#
#  fk_rails_feacd7ba86  (profile_id => profiles.id)
#

class Education < ActiveRecord::Base
  validates :school,:year_from, :year_to, :description, presence: true
  validate :greater_year_from
  belongs_to :profile
  def greater_year_from
    errors.add(:year_to, "Starting year is greater") if self.year_from > self.year_to
  end
end
