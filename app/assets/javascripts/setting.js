$(document).ready(function(){
  $('.billing-methods').on('click', function(){
    $('.open').hide();
    $('.billing-section-div').css({'display':'block'});
    $('.billing-section-div').addClass('open');
  });
  $('.membership-conncts').on('click', function(){
      $('.open').hide();
    $('.membership-section-div').css({'display':'block'});
      $('.membership-section-div').addClass('open');
  });
  $('.contact-info').on('click', function(){
      $('.open').hide();
    $('.contactinfo-section-div').css({'display':'block'});
      $('.contactinfo-section-div').addClass('open');
  });
  $('.change-password').on('click', function(){
      $('.open').hide();
    $('.changepassword-section-div').css({'display':'block'});
      $('.changepassword-section-div').addClass('open');
  });
  $('.change-security').on('click', function(){
      $('.open').hide();
    $('.changesecurity-section-div').css({'display':'block'});
      $('.changesecurity-section-div').addClass('open');
  });
  $('.profilesetting').on('click', function(){
      $('.open').hide();
    $('.profilesetting-section-div').css({'display':'block'});
      $('.profilesetting-section-div').addClass('open');
  });
  $('.getpaid-section').on('click', function(){
      $('.open').hide();
    $('.getpaid-section-div').css({'display':'block'});
      $('.getpaid-section-div').addClass('open');
  });
  $('.notificatin-section').on('click', function(){
      $('.open').hide();
    $('.notificatin-section-div').css({'display':'block'});
      $('.notificatin-section-div').addClass('open');
  });
  $('.classes-info-link1').on('click', function(){
      $('.this').addClass('.active');
        $('.classes-info-link2').removeClass('.active');
    // $(this).css({'color':'#379ED4', 'background-color':'#D7D8D9'});
    $('.open').hide();
    $('.upcoming-classes').css({'display':'block'});
    $('.upcoming-classes').addClass('open');
  });
  $('.classes-info-link2').on('click', function(){
      $('.this').addClass('.active');
        $('.classes-info-link1').removeClass('.active');
    // $(this).css({'color':'#379ED4', 'background-color':'#D7D8D9'});
      $('.open').hide();
    $('.completed-class').css({'display':'block'});
      $('.completed-class').addClass('open');
  });


});
