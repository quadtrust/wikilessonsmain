$(->
  $('.rateYo').rateYo({
    onSet: (rating, rateYoInstance)->
      $('#' + rateYoInstance.node.attributes['data-id'].nodeValue + '_rating').val(rating);
  });
)