# == Schema Information
#
# Table name: profiles
#
#  id                   :integer          not null, primary key
#  firstname            :string(255)
#  lastname             :string(255)
#  avatar               :string(255)
#  phone1               :string(255)
#  phone2               :string(255)
#  about                :text(65535)
#  custom_video         :string(255)
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  profile_status       :integer
#  verified             :boolean          default(FALSE)
#  rate                 :decimal(16, 2)
#  address              :string(255)
#  native_language      :string(255)
#  additional_languages :string(255)
#  standard             :integer
#  stripe_customer_id   :string(255)
#  average_rating       :float(24)        default(0.0)
#  gender               :integer          default(0)
#  availability         :integer          default(1)
#  zone                 :string(255)
#
# Indexes
#
#  index_profiles_on_stripe_customer_id  (stripe_customer_id)
#  index_profiles_on_user_id             (user_id)
#
# Foreign Keys
#
#  fk_rails_e424190865  (user_id => users.id)
#

class Profile < ActiveRecord::Base
  acts_as_taggable_on :subjects
  mount_uploader :avatar, AvatarUploader
  belongs_to :user
  has_many :educations
  has_many :experiences

  # validates_presence_of :firstname, :phone1

  # This is for the skill section that would be used to determine what the teacOher will be teaching.


  #   Creating the profile completeness
  PROFILE_COMPLETENESS_TEACHER = %w(firstname avatar phone1 about rate address native_language additional_languages standard educations experiences)
  PROFILE_COMPLETENESS_STUDENT = %w(firstname avatar phone1 about address native_language additional_languages standard)

  STANDARD = {
      0 => 'Standard 1',
      1 => 'Standard 2',
      2 => 'Standard 3',
      3 => 'Standard 4'
  }

  GENDER = {
      0 => 'Male',
      1 => 'Female'
  }

  GENDER_GET = {
      'male' => 0,
      'female' => 1
  }

  AVAILABILITY = {
      0 => 'Offline',
      1 => 'Online'
  }

  AVAILABILITY_GET = {
      'online' => 1,
      'offline' => 0
  }

  before_update :update_profile_progress
  after_create :create_stripe_customer

  def fullname
    "#{self.firstname} #{self.lastname}"
  end

  def video
    link = self.custom_video.split('=').last if self.custom_video
    if link.empty?
      link = self.custom_video.split('/').last if self.custom_video
    end
    link
  end

  private

  def update_profile_progress
    progress = 0
    if self.user.has_role? :teacher
      PROFILE_COMPLETENESS_TEACHER.each do |field|
        unless self.send(field).blank?
          progress = progress + 1
        end
      end
      self.profile_status = ( progress.to_f / PROFILE_COMPLETENESS_TEACHER.length * 100).to_i
    else
      PROFILE_COMPLETENESS_STUDENT.each do |field|
        unless self[field].blank?
          progress = progress + 1
        end
      end
      self.profile_status = ( progress.to_f / PROFILE_COMPLETENESS_STUDENT.length * 100).to_i
    end
  end

  def create_stripe_customer
    customer = Stripe::Customer.create(email: self.user.email)
    self.update(stripe_customer_id: customer.id)
  end
end
