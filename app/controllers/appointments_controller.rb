# == Schema Information
#
# Table name: appointments
#
#  id               :integer          not null, primary key
#  description      :text(65535)
#  accepted         :boolean          default(FALSE)
#  completed        :boolean          default(FALSE)
#  student_id       :integer
#  teacher_id       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  topic            :string(255)
#  class_valid      :boolean          default(FALSE)
#  appointment_date :datetime
#
# Indexes
#
#  index_appointments_on_student_id  (student_id)
#  index_appointments_on_teacher_id  (teacher_id)
#

include ApplicationHelper
class AppointmentsController < ApplicationController

  before_action :authenticate_user!
  before_action :load_user, only: [:create]
  before_action :set_appointment, only: [:cancel_class, :accept_class, :start_class]


  # This is the function that would be used by the class-book vue component after
  # it mounts to check if the user has booked any class with the current teacher.
  # This can be achieved by a Ajax Get call to : /classinfo/:id (where id is the teacher id.)
  # The user needs to be authenticated to use this method.
  def class_info
    @user = User.find(params[:id])
    if @user.has_role? :teacher
      # do something.

      if current_user == @user
        showuser = false
      else
        showuser = true
      end

      classinfo = current_user.student_appointments.where(teacher_id: params[:id]).where('class_status != ?', Appointment::CLASS_STATUS_GET[:ended]).last

      if classinfo.nil?
        render json: {
            username: @user.username,
            showuser: showuser,
            class_booked: false,
            appointment_date: '',
            class_accepted: false,
            payment: false,
            class_valid: false,
            completed: 0
        }
      else
        render json: {
            username: @user.username,
            class_valid: classinfo.class_valid,
            showuser: showuser,
            class_booked: true,
            appointment_date: classinfo.appointment_date.strftime("%A, %d %b %Y %l:%M %p"),
            payment: classinfo.payment.paid,
            completed: classinfo.completed
        }
      end

    else
      render json: {
          error: 'This is only allowed for a teacher.'
      }
    end
  end

  def accept_class
    if @appointment.update(accepted: true)
      # IMPORTANT: Do not remove this variable
      teacher = @appointment.teacher
      send_notification(ERB.new(CLASS_ACCEPTED_MESSAGE).result(binding), @appointment.student, '/')
      flash[:notice] = 'Your class has been accepted'
      redirect_to :back
    end

  end

  def cancel_class
    if @appointment.class_valid
      if current_user.has_role? :teacher
          Refund.create(appointment_id: @appointment.id, amount: @appointment.payment.teacher_pay, user: @appointment.student)
      else
        if @appointment.appointment_date - 2.days > Time.now
          Refund.create(appointment_id: @appointment.id, amount: @appointment.payment.teacher_pay, user: @appointment.student)
        end
      end
      @appointment.update(completed: 2)
    end
    if @appointment.update(completed: 2)
      # IMPORTANT: Do not remove this variable
      if current_user.has_role? :teacher
        user = @appointment.teacher
        notify = @appointment.student
      else
        user = @appointment.student
        notify = @appointment.teacher
      end
      send_notification(ERB.new(CLASS_CANCEL_MESSAGE).result(binding), notify, '/')
      flash[:notice] = 'Your class has been canceled'
      redirect_to :back
    end
  end


  def create
    # if only the current user is a student and has a role of student.
    if current_user.has_role? :student
      # if the user whom the student is trying to create an appointment is a teacher. ( The user is loaded from the before_action )
      if @user.has_role? :teacher
        @appointment = Appointment.new(appointment_params)
        @appointment.student_id = current_user.id
        @appointment.teacher_id = params[:profile_id]
        if @appointment.save
          teacher = @appointment.teacher
          # IMPORTANT: Do not remove this variable
          student = @appointment.student
          send_notification(ERB.new(CLASS_CREATED_MESSAGE).result(binding), teacher, '/')
          render json: @appointment
        else
          render json: {
              error: 'The model could not be saved.'
          }
        end
      else
        render json: {
            error: 'You are not allowed to do this action.'
        }
      end
    else
      render json: {
          error: 'You are not allowed to do this action.'
      }
    end
  end

  def start_class
    @appointment["started_by_#{current_role}"] = true
    @appointment.save
    if @appointment.started_by_student and @appointment.started_by_teacher
      @appointment.update(started_at: Time.now, class_status: 2)
      @appointment.set_job_for_end
    end
    redirect_to :back
  end

  private

  def set_appointment
    @appointment = Appointment.find params[:id]
  end

  def load_user
    @user = Profile.find(params[:profile_id]).user
  end

  def appointment_params
    params.require(:appointment).permit(:description, :accepted, :topic, :profile_id, :appointment_date, :class_hours)
  end

end