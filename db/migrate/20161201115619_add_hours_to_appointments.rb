class AddHoursToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :class_hours, :integer
  end
end
