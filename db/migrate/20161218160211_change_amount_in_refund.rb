class ChangeAmountInRefund < ActiveRecord::Migration
  def change
    change_column :refunds, :amount, :decimal, scale: 2, precision: 10
  end
end
