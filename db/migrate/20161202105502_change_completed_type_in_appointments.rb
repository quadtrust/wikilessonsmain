class ChangeCompletedTypeInAppointments < ActiveRecord::Migration
  def change
    change_column :appointments, :completed, :integer, default: 0
  end
end
