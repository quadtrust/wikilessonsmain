function calculate()
{
    var a = document.getElementById("value_a").value;
    var servicetax = a * (12 / 100);
    document.getElementById('serviceval').value= -servicetax.toFixed(2);
    var finalamount = (a - servicetax);
    document.getElementById('final').value = finalamount.toFixed(2);
}


$(document).ready(function(){
    $('#value_a').on('input',function(e){
        e.preventDefault();
        calculate();
    });


    $('#save_newrate_button').on('click', function () {
        var finalamount = document.getElementById("value_a").value;

        $.ajax({
           type: 'PUT',
            url: '/profiles/' + $(this).data('profileid')+ '.js',
            data: {
                profile: {
                    rate: finalamount
                }
            },
            dataType: 'JSON',
            success: function (data) {
                console.log('this is a demo');
            }

        });

    })
});