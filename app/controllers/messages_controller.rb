class MessagesController < ApplicationController

  def index
    @conversations = current_user.mailbox.conversations
  end

  def new
    @user = User.find params[:id]
    redirect_to :back and return if current_user.id == @user.id
    redirect_to root_path and return unless @user.has_role? :teacher
    redirect_to messages_path if current_user.mailbox.conversations_with(@user).count > 0
  end

  def create
    @user = User.find params[:id]
    if params[:message][:message].length > 0
      redirect_to root_path unless @user.has_role? :teacher
      redirect_to messages_path if current_user.mailbox.conversations_with(@user).count > 0
      current_user.send_message(@user, params[:message][:message], 'Subject')
      redirect_to messages_path
    else
      render 'new'
    end
  end

  def reply
    if params[:message][:message].length > 0
      @conversation = current_user.mailbox.conversations.where(id: params[:id]).last
      current_user.reply_to_conversation(@conversation, params[:message][:message])
    end
    redirect_to messages_path
  end

  def get_messages_list
    @conversation = current_user.mailbox.conversations.where(id: params[:id]).last
  end

end
