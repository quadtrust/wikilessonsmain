class CardsController < ApplicationController
  before_action :authenticate_user!


  def new
  end

  def create
    card = Card.new(card_params)
    card.user = current_user
    if card.save
      redirect_to root_path
    end
  end

  def destroy
  end

  private

  def card_params
    params.require(:card).permit(:token)
  end

end
