// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require ajax_prefilter
//= require jquery.datetimepicker
//= require home
//= require profileview
//= require dashboardindex
//= require paloma
//= require paloma_start
//= require vue
//= require main_vue
//= require bootstrap-tagsinput.js
//= require cards
//= require jquery.card
//= require messages
//= require book_class
//= require class_history
//= require jquery.rateyo.min
//= require_self



$(document).ready(function() {
    Stripe.setPublishableKey('pk_test_RJE4aLLPOlJJ2exEZbxyuHtO');
    $('.add_education').click(function(){
        $('#new_education')[0].reset();
        $('#education_form').toggle("slow");
    });


    $('.add_experience').click(function(){
        $('#new_experience')[0].reset();
        $('#experience_form').toggle("slow");
    });

    $('.add_skill').click(function(){

        $('#skill_form').toggle("slow");
    });

});