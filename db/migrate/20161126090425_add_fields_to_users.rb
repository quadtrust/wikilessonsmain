class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :avatar, :string
    add_column :users, :phone1, :string
    add_column :users, :phone2, :string
    add_column :users, :address, :text
    add_column :users, :about, :text
    add_column :users, :custom_video, :string
  end
end
