# == Schema Information
#
# Table name: cards
#
#  id          :integer          not null, primary key
#  brand       :string(255)
#  country     :string(255)
#  month       :string(255)
#  year        :string(255)
#  last_digits :string(255)
#  default     :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  token       :string(255)
#
# Indexes
#
#  index_cards_on_token    (token)
#  index_cards_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_8ef7749967  (user_id => users.id)
#

class Card < ActiveRecord::Base
  belongs_to :user

  after_create :update_stripe

  private

  def update_stripe
    customer = Stripe::Customer.retrieve(self.user.profile.stripe_customer_id)
    card = customer.sources.create(source: self.token)
    self.user.cards.each do |c|
      c.update(default: false)
    end
    self.update(brand: card.brand,
                country: card.country,
                month: card.exp_month,
                year: card.exp_year,
                last_digits: card.last4,
                default: true
    )
    customer.default_source = card.id
    customer.save
  end
end
