class EducationsController < ApplicationController

  before_action :set_profile, only: [:create]


  def create
    @education = @profile.educations.new(education_params)

    respond_to do |format|
      if @education.save
        profile = @education.profile
        profile.update(rate: profile.rate)
        format.html { redirect_to :back, notice: 'Education was successfully added' }
      else
        format.html { redirect_to :back, notice: 'Please fill all the filled correctly.' }
      end
    end
  end

  private

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def set_education
    @education = Education.find(params[:id])
  end

  def education_params
    params.require(:education).permit(:school, :year_from, :year_to, :description)
  end
end
