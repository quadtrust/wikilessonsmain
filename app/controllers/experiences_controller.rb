class ExperiencesController < ApplicationController
  before_action :set_profile, only: [:create]


  def create
    @experience = @profile.experiences.new(experience_params)

    respond_to do |format|
      if @experience.save
        profile = @experience.profile
        profile.update(rate: profile.rate)
        format.html { redirect_to :back, notice: 'Education was successfully added' }
      else
        format.html { redirect_to :back, notice: 'Please fill all the filled correctly.' }
      end
    end
  end

  private

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def set_experience
    @experience = Experience.find(params[:id])
  end

  def experience_params
    params.require(:experience).permit(:subject, :experience_time, :description)
  end
end