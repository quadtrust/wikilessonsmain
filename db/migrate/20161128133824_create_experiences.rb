class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.string :subject
      t.text :description
      t.integer :experience_time
      t.belongs_to :profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
