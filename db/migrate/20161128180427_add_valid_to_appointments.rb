class AddValidToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :valid, :boolean, default: false
  end
end
