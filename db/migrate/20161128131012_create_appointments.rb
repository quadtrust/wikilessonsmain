class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.text :description
      t.date :appointment_date
      t.time :appointment_time
      t.boolean :accepted, default: false
      t.boolean :completed, default: false
      t.references :student, index: true
      t.references :teacher, index: true

      t.timestamps null: false
    end
  end
end
