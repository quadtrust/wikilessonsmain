# == Schema Information
#
# Table name: testimonials
#
#  id             :integer          not null, primary key
#  description    :text(65535)
#  rating         :float(24)
#  appointment_id :integer
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  to_user_id     :integer
#
# Indexes
#
#  index_testimonials_on_appointment_id  (appointment_id)
#  index_testimonials_on_user_id         (user_id)
#
# Foreign Keys
#
#  fk_rails_4d3e46b658  (user_id => users.id)
#  fk_rails_fae0c4defc  (appointment_id => appointments.id)
#

class Testimonial < ActiveRecord::Base
  belongs_to :appointment
  belongs_to :user
  belongs_to :to_user, class_name: 'User'

  validates :description, :rating, :appointment_id, :user_id, :to_user_id, presence: true

  after_create :update_average_ratings

  private

  def update_average_ratings
    if self.user.has_role? :teacher
      student = self.appointment.student
      student.profile.update(average_rating: student.testimonials.average(:rating))
    else
      teacher = self.appointment.teacher
      teacher.profile.update(average_rating: teacher.testimonials.average(:rating))
    end
  end
end
