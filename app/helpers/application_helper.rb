module ApplicationHelper

  def default_card(user = false)
    user = current_user unless user
    user.cards.where(default: true).last
  end

  def opposite_role(user = false)
    user = current_user unless user
    if user.has_role? :teacher
      'student'
    else
      'teacher'
    end
  end

  def current_role(user = false)
    user = current_user unless user
    if user.has_role? :teacher
      'teacher'
    else
      'student'
    end
  end

  def get_other_participant(partcipants)
    if partcipants[0].id == current_user.id
      partcipants[1]
    else
      partcipants[0]
    end
  end

  def time_in_zone(time, user = nil)
    user = current_user unless user
    return time.in_time_zone(user.profile.zone) if user.profile.zone
    time
  end

  def get_avatar(profile = nil, version = nil)
    profile = current_user.profile unless profile
    if profile.avatar_url
      profile.avatar_url(version)
    else
      asset_path('avatar.jpeg')
    end
  end

  def get_current_seo_route
    url = request.path
    if url == '/'
      '_'
    else
      url[1..-1].gsub(/\//, '_')
    end
  end

  def get_seo_description
    current_route = get_current_seo_route
    if $seo[current_route]
      $seo[get_current_seo_route]['description']
    else
      current_route
    end
  end

  def get_seo_keywords
    current_route = get_current_seo_route
    if $seo[current_route]
      $seo[current_route]['keywords']
    else
      current_route
    end
  end

  def get_seo_title
    current_route = get_current_seo_route
    if $seo[current_route]
      $seo[current_route]['title']
    else
      current_route
    end
  end

  def available_earnings
    number_to_currency(Earning.where(user: current_user).sum(:amount))
  end

end
