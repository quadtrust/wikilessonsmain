# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  username               :string(255)
#  role                   :integer
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_username              (username) UNIQUE
#

class User < ActiveRecord::Base
  # This is for the user roles.
  rolify
  acts_as_messageable
  attr_accessor :login
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  validates :username, uniqueness: true
  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, multiline: true

  ROLES = {
      0 => 'Student',
      1 => 'Teacher'
  }

  after_create :add_specified_role
  before_create :build_default_profile

  # Write all the associations below.
  has_one :profile

  # Adding the association for the appointments
  has_many :student_appointments, class_name: 'Appointment', foreign_key: 'student_id'
  has_many :teacher_appointments, class_name: 'Appointment', foreign_key: 'teacher_id'
  # This is the section for the payment table.
  has_many :student_payments, class_name: 'Payment', foreign_key: 'student_id'
  has_many :teacher_payments, class_name: 'Payment', foreign_key: 'teacher_id'
  has_many :cards
  has_many :notifications

  has_many :testimonials_given, class_name: 'Testimonial'
  has_many :testimonials, class_name: 'Testimonial', foreign_key: 'to_user_id'
  has_many :refunds
  has_many :earnings

  def add_specified_role
    if self.role == 0 || self.role.nil?
      self.add_role(:student)
    elsif self.role == 1
      self.add_role(:teacher)
    end
  end

  def name
    self.profile.fullname
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(['lower(username) = :value OR lower(email) = :value', {:value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  private

  def build_default_profile
    # build default profile instance. Will use default params.
    # The foreign key to the owning User model is set automatically
    build_profile
    true # Always return true in callbacks as the normal 'continue' state
    # Assumes that the default_profile can **always** be created.
    # or
    # Check the validation of the profile. If it is not valid, then
    # return false from the callback. Best to use a before_validation
    # if doing this. View code should check the errors of the child.
    # Or add the child's errors to the User model's error array of the :base
    # error item
  end

end
