$(->
  $('.filter-event').on 'change', (e)->
    gender = ['male', 'female', 'no-filter-gender']
    ratings = ['4+', 'no-filter-rating']
    availability = ['online', 'offline']
    selected_value = $(this).val()
    console.log(selected_value);
    if(gender.indexOf(selected_value) > -1)
      $('#teacher_gender').val(selected_value)
    if(ratings.indexOf(selected_value) > -1)
      $('#teacher_rating').val(selected_value)
    if(availability.indexOf(selected_value) > -1)
      $('#teacher_availability').val(selected_value)

    $('#book-class-form').get(0).submit();
)
