class DashboardController < ApplicationController
  before_action :authenticate_user!, except: [:book_class_search, :book_class]
  def index
    @user = current_user
    if @user.has_role? :student

      @awating_confirmation = @user.student_appointments.where(completed: 0, accepted: false)

      @awating_payment = @user.student_appointments.where(completed: 0, accepted: true, class_valid: false)
      @registered_class = @user.student_appointments.where(completed: 0, accepted: true, class_valid: true)
    else
      @awating_confirmation = @user.teacher_appointments.where(completed: 0, accepted: false)

      @awating_payment = @user.teacher_appointments.where(completed: 0, accepted: true, class_valid: false)
      @registered_class = @user.teacher_appointments.where(completed: 0, accepted: true, class_valid: true)

    end
  end

  def settings
  end

  def book_class
    if current_user != nil and current_user.profile.profile_status != 100
      redirect_to edit_profile_path(current_user.profile), notice: 'You need to complete your profile first.'
    end
    @teachers = []
    @relevant = false
    params[:teacher] = {}
    params[:teacher][:gender] = 'no-filter-gender'
    params[:teacher][:rating] = 'no-filter-rating'
    params[:teacher][:availability] = 'online'
  end

  def book_class_search
    gender = params[:teacher][:gender]
    rating = params[:teacher][:rating]
    availability = Profile::AVAILABILITY_GET[params[:teacher][:availability]]
    availability = Profile::AVAILABILITY_GET['online'] unless availability
    standard = params[:teacher][:standard]
    @teachers = Profile.where(availability: availability).tagged_with(params[:teacher][:subject], profile_status: 100)
    if standard
      @teachers.where(standard: standard)
    end
    if rating == '4+'
      @teachers = @teachers.where('average_rating >= 4.0')
    end
    if gender == 'male' || gender == 'female'
      @teachers = @teachers.where(gender: Profile::GENDER_GET[gender])
    end
    @relevant = true
    render 'book_class'
  end

  def class_history
    if current_user.has_role? :teacher
      @classes = Appointment.where(teacher_id: current_user.id, class_status: 3)
    else
      @classes = Appointment.where(student_id: current_user.id, class_status: 3)
    end
  end

end
