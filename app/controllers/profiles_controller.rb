# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  firstname    :string(255)
#  lastname     :string(255)
#  avatar       :string(255)
#  phone1       :string(255)
#  phone2       :string(255)
#  about        :text(65535)
#  custom_video :string(255)
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ProfilesController < ApplicationController

  before_action :set_profile, only: [:show, :update, :edit]
  before_action :authenticate_user!, except: [:show]

  def show
    if current_user == @profile.user
      if @profile.profile_status < 81
        redirect_to edit_profile_path(@profile), notice: 'Your profile needs to be 100% complete.'
      end
    elsif @profile.profile_status != 100
      redirect_to root_path, notice: 'They haven\'t completed their profile.'
    end

    @education = @profile.educations.new

    @all_education = @profile.educations.all
    @experience = @profile.experiences.all
    @two_subject = @profile.experiences.first(2)
    @new_experience = @profile.experiences.new
    @tags = @profile.subjects
    @total_hours = Appointment.where(teacher_id: @profile.user_id, class_status: 3).sum(:class_hours)
    @total_classes = Appointment.where(teacher_id: @profile.user_id, class_status: 3).count(:id)

    @is_teacher = @profile.user.has_role? :teacher
  end


  def edit
    redirect_to root_path unless current_user.id == @profile.user_id
  end


  def update
    if @profile.update(profile_params)
      respond_to do |format|
        format.html {
          flash[:notice] = 'Profile Updated Successfully'
          redirect_to @profile
        }

        # format.json { render :show, status: :ok, location: @profile }

        format.js { render json: @profile }
      end
    else
      respond_to do |format|
        format.html{
          render :edit
        }

        format.json { render json: @profile.errors, status: :unprocessable_entity }
        format.js { render json: @profile.errors, status: :unprocessable_entity }

      end

    end
  end

  private

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def profile_params
    params.require(:profile).permit(:firstname, :lastname, :about, :avatar, :custom_video, :phone1, :phone2, :tag_list, :address, :native_language, :additional_languages, :standard, :rate, :subject_list, :availability, :zone)
  end

end