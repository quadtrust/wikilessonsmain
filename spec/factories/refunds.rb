FactoryGirl.define do
  factory :refund do
    status false
    amount 1
    comment "MyText"
    appointment nil
  end
end
