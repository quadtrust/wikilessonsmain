# == Schema Information
#
# Table name: payments
#
#  id             :integer          not null, primary key
#  student_id     :integer
#  teacher_id     :integer
#  teacher_pay    :decimal(16, 2)
#  profit_pay     :decimal(16, 2)
#  total_pay      :decimal(16, 2)
#  paid           :boolean          default(FALSE)
#  comment        :text(65535)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :integer
#  stripeid       :string(255)
#
# Indexes
#
#  index_payments_on_appointment_id  (appointment_id)
#  index_payments_on_student_id      (student_id)
#  index_payments_on_teacher_id      (teacher_id)
#
# Foreign Keys
#
#  fk_rails_30cd1cf5da  (appointment_id => appointments.id)
#

class Payment < ActiveRecord::Base
  belongs_to :student, class_name: 'User'
  belongs_to :teacher, class_name: 'User'
  belongs_to :appointment

  after_update :make_class_valid, if: :paid_changed?

  def make_class_valid
    self.appointment.update!(class_valid: true)
  end

  validates_presence_of :student_id, :teacher_id
end
