# == Route Map
#
#                   Prefix Verb   URI Pattern                                      Controller#Action
#                dashboard GET    /dashboard(.:format)                             dashboard#index
#         new_user_session GET    /users/sign_in(.:format)                         devise/sessions#new
#             user_session POST   /users/sign_in(.:format)                         devise/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)                        devise/sessions#destroy
#            user_password POST   /users/password(.:format)                        devise/passwords#create
#        new_user_password GET    /users/password/new(.:format)                    devise/passwords#new
#       edit_user_password GET    /users/password/edit(.:format)                   devise/passwords#edit
#                          PATCH  /users/password(.:format)                        devise/passwords#update
#                          PUT    /users/password(.:format)                        devise/passwords#update
# cancel_user_registration GET    /users/cancel(.:format)                          devise/registrations#cancel
#        user_registration POST   /users(.:format)                                 devise/registrations#create
#    new_user_registration GET    /users/sign_up(.:format)                         devise/registrations#new
#   edit_user_registration GET    /users/edit(.:format)                            devise/registrations#edit
#                          PATCH  /users(.:format)                                 devise/registrations#update
#                          PUT    /users(.:format)                                 devise/registrations#update
#                          DELETE /users(.:format)                                 devise/registrations#destroy
#                     root GET    /                                                dashboard#index
#     unauthenticated_root GET    /                                                home#index
#     profile_appointments POST   /profiles/:profile_id/appointments(.:format)     appointments#create
#  new_profile_appointment GET    /profiles/:profile_id/appointments/new(.:format) appointments#new
#      profile_appointment PATCH  /profiles/:profile_id/appointments/:id(.:format) appointments#update
#                          PUT    /profiles/:profile_id/appointments/:id(.:format) appointments#update
#             edit_profile GET    /profiles/:id/edit(.:format)                     profiles#edit
#                  profile GET    /profiles/:id(.:format)                          profiles#show
#                          PATCH  /profiles/:id(.:format)                          profiles#update
#                          PUT    /profiles/:id(.:format)                          profiles#update
#

Rails.application.routes.draw do

  get 'dashboard' => 'dashboard#index'
  get 'settings' => 'dashboard#settings'
  get 'book-class' => 'dashboard#book_class'
  post 'book-class' => 'dashboard#book_class_search'
  get 'class-history' => 'dashboard#class_history'

  devise_for :users

  authenticated :user do
    root 'dashboard#index', as: :root
  end

  unauthenticated :user do
    root 'home#index', as: :unauthenticated_root
  end

  resources :profiles, only: [:show, :update, :edit] do
    resources :educations , only: [ :update, :edit, :create]
    resources :experiences , only: [ :update, :edit, :create]
    # creating the appointments for the user.
    resources :appointments, only: [:new, :update, :create] do
      resources :payments, only: [:new, :create] do
        member do
          get 'make'
          post 'confirm'
        end
      end
      member do
        put 'accept_class'
        put 'cancel_class'
        put 'start_class'
      end
    end

    resources :cards, only: [:new, :create, :destroy]

    get 'payments/pending' => 'payments#pending', as: :pending_payments
    post 'payments#withdraw' => 'payments#withdraw', as: :withdraw_payments
  end

  resources :appointments, only: [] do
    resources :testimonials, only: [:create]
  end

  get 'messages' => 'messages#index', as: :messages
  get 'messages/:id/new' => 'messages#new', as: :new_message
  post 'messages/:id' => 'messages#create', as: :create_message
  post 'messages/:id/reply' => 'messages#reply', as: :reply_message
  get 'messages/:id/conversation' => 'messages#get_messages_list'

  get 'aboutus' => 'home#aboutus'
  get 'faq' => 'home#faq'
  get 'course' => 'home#course'
  get 'whywikilesson' => 'home#whywikilesson'
  get 'blog' => 'home#blog'
  get 'contactus' => 'home#contactus'
  get 'howitwork' => 'home#howitwork'

  # get the user class info.
  get 'classinfo/:id' => 'appointments#class_info'

end
