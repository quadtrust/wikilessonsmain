class DeleteFieldsFromAppointments < ActiveRecord::Migration
  def change
    remove_column :appointments, :appointment_date, :date
    remove_column :appointments, :appointment_time, :time
  end
end
