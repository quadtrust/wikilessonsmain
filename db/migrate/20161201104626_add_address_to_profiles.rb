class AddAddressToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :address, :string
    add_column :profiles, :native_language, :string
    add_column :profiles, :additional_languages, :string
    add_column :profiles, :standard, :integer
  end
end
