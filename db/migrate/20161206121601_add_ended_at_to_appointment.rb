class AddEndedAtToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :ended_at, :datetime
  end
end
