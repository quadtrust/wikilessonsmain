class AddClassStartedAtToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :started_at, :datetime
  end
end
