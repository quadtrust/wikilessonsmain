class AddClassStartFlagsToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :started_by_student, :boolean, default: false
    add_column :appointments, :started_by_teacher, :boolean, default: false
  end
end
