class CreateEarnings < ActiveRecord::Migration
  def change
    create_table :earnings do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.decimal :amount, scale: 2, precision: 10
      t.boolean :withdrawn, default: false

      t.timestamps null: false
    end
  end
end
