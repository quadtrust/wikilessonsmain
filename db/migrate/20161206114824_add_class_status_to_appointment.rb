class AddClassStatusToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :class_status, :integer, default: 0
  end
end
