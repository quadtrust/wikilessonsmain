class ChangeValidInAppointments < ActiveRecord::Migration
  def change
    rename_column :appointments, :valid, :class_valid
  end
end
