# == Schema Information
#
# Table name: refunds
#
#  id             :integer          not null, primary key
#  status         :boolean
#  amount         :decimal(10, 2)
#  comment        :text(65535)
#  appointment_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#
# Indexes
#
#  index_refunds_on_appointment_id  (appointment_id)
#  index_refunds_on_user_id         (user_id)
#
# Foreign Keys
#
#  fk_rails_3ddfe85141  (appointment_id => appointments.id)
#  fk_rails_7055819eb7  (user_id => users.id)
#

class Refund < ActiveRecord::Base
  belongs_to :appointment
  belongs_to :user

  validates :amount, :appointment_id, :user_id, presence: true
end
