class AddRateToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :rate, :decimal, precision: 16, scale: 2
  end
end
