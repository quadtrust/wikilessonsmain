var stripeResponseHandler = function(status, response){
    var $form = $('#real-stripe-form');
    var $old_form = $('#stripe-form');
    if(response.error){
        $('#error').html(response.error.message);
        $old_form.find('.submit').prop('disabled', false);
    }else{
        var token = response.id;

        $form.append($('<input type="hidden" name="card[token]">').val(token));
        $form.get(0).submit();
    }
};

Paloma.controller('Cards', {
    new: function(){
        var $form = $('#stripe-form');
        $form.submit(function(event){
            $form.find('.submit').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
            return false;
        });

        $form.card({
            container: '#card-container',
            formSelectors: {
                expiryInput: 'input#exp-month, input#exp-year'
            }
        });
    }
});