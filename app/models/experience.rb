# == Schema Information
#
# Table name: experiences
#
#  id              :integer          not null, primary key
#  subject         :string(255)
#  description     :text(65535)
#  experience_time :integer
#  profile_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_experiences_on_profile_id  (profile_id)
#
# Foreign Keys
#
#  fk_rails_0229332b88  (profile_id => profiles.id)
#

class Experience < ActiveRecord::Base
  validates :subject, :description, :experience_time, presence: true
  belongs_to :profile
end
