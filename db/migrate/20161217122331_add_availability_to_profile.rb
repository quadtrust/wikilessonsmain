class AddAvailabilityToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :availability, :integer, default: 1
  end
end
