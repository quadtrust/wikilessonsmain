# == Schema Information
#
# Table name: payments
#
#  id             :integer          not null, primary key
#  student_id     :integer
#  teacher_id     :integer
#  teacher_pay    :decimal(16, 2)
#  profit_pay     :decimal(16, 2)
#  total_pay      :decimal(16, 2)
#  paid           :boolean          default(FALSE)
#  comment        :text(65535)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :integer
#  stripeid       :string(255)
#
# Indexes
#
#  index_payments_on_appointment_id  (appointment_id)
#  index_payments_on_student_id      (student_id)
#  index_payments_on_teacher_id      (teacher_id)
#
# Foreign Keys
#
#  fk_rails_30cd1cf5da  (appointment_id => appointments.id)
#

class PaymentsController < ApplicationController
  before_action :authenticate_user!

  def new
  end

  def create
    # Amount in cents
    @amount = 500

    @payment = Payment.find(params[:id])

    customer = Stripe::Customer.create(
        :email => params[:stripeEmail],
        :source  => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
        :customer    => customer.id,
        :amount      => @amount,
        :description => 'Rails Stripe customer',
        :currency    => 'usd'
    )

    if charge['paid'] == true
      @payment.update(paid: true)
    end
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end

  def make
    @payment = Payment.find params[:id]
  end

  def confirm
    payment = Payment.find params[:id]
    charge = Stripe::Charge.create(
                      amount: (payment.total_pay.to_f * 100).to_i,
                      currency: 'usd',
                      customer: current_user.profile.stripe_customer_id
    )
    if charge.paid
      # IMPORTANT: Do not remove this variable
      student = payment.appointment.student
      send_notification(ERB.new(CLASS_PAID_MESSAGE).result(binding), payment.appointment.teacher, '/')
      payment.update(paid: true)
      payment.appointment.set_job_for_start
    end
    redirect_to root_path
  end

  def pending
  end

  def withdraw
    available = Earning.where(user: current_user).sum(:amount)
    if available > Earning::WITHDRAWAL_MIN_LIMIT
      flash[:success] = 'Request sent'
    else
      flash[:error] = "You cannot withdraw before #{Earning::WITHDRAWAL_MIN_LIMIT}"
    end
    redirect_to :back
  end

end
