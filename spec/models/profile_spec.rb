# == Schema Information
#
# Table name: profiles
#
#  id                   :integer          not null, primary key
#  firstname            :string(255)
#  lastname             :string(255)
#  avatar               :string(255)
#  phone1               :string(255)
#  phone2               :string(255)
#  about                :text(65535)
#  custom_video         :string(255)
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  profile_status       :integer
#  verified             :boolean          default(FALSE)
#  rate                 :decimal(16, 2)
#  address              :string(255)
#  native_language      :string(255)
#  additional_languages :string(255)
#  standard             :integer
#
# Indexes
#
#  index_profiles_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_e424190865  (user_id => users.id)
#

require 'rails_helper'


RSpec.describe Profile, type: :model do
  it { should have_many(:educations) }
  it { should validate_presence_of(:firstname) }
  it { should validate_presence_of(:phone1) }
end
