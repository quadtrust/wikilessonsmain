class AddStripeCustomerIdToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :stripe_customer_id, :string
    add_index :profiles, :stripe_customer_id
  end
end
