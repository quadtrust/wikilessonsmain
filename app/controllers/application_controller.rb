class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  http_basic_authenticate_with name: 'wiki', password: 'wiki123' if Rails.env == 'production'

  # This is for the added parameters that would go for the other controllers
  before_action :configure_permitted_parameters, if: :devise_controller?

  #Appointment notification messages
  CLASS_CREATED_MESSAGE = 'You have received a new class request from <%= student.name %>.'
  CLASS_ACCEPTED_MESSAGE = '<%= teacher.name %> has accepted your class request.'
  CLASS_PAID_MESSAGE = '<%= student.name %> has paid for your class.'
  CLASS_CANCEL_MESSAGE = '<%= user.name %> has cancelled your class.'

  FIREBASE = Firebase::Client.new('https://wiki-lessons.firebaseio.com/', Rails.application.secrets.firebase_database_key)
  MAX_NOTIFICATIONS_LIMIT = 10

  protected

  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me, :role]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def send_notification(str, to, link = nil)
    Notification.create(user: to, text: str, link: link)
    key = "notifications-#{to.id}"
    FIREBASE.push(key, message: str, link: link)
    notifications = FIREBASE.get(key).body
    if notifications.size > MAX_NOTIFICATIONS_LIMIT
      delete_elem_count = notifications.size - MAX_NOTIFICATIONS_LIMIT
      i = 0
      notifications.each_key do |_key|
        if i < delete_elem_count
          FIREBASE.delete("#{key}/#{_key}")
          i += 1
        else
          break
        end
      end
    end
  end

end
