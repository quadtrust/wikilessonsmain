# == Schema Information
#
# Table name: notifications
#
#  id         :integer          not null, primary key
#  text       :text(65535)
#  link       :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_notifications_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_b080fb4855  (user_id => users.id)
#

class Notification < ActiveRecord::Base
  belongs_to :user
end
