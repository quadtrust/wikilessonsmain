# == Schema Information
#
# Table name: payments
#
#  id             :integer          not null, primary key
#  student_id     :integer
#  teacher_id     :integer
#  teacher_pay    :decimal(16, 2)
#  profit_pay     :decimal(16, 2)
#  total_pay      :decimal(16, 2)
#  paid           :boolean          default(FALSE)
#  comment        :text(65535)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :integer
#
# Indexes
#
#  index_payments_on_appointment_id  (appointment_id)
#  index_payments_on_student_id      (student_id)
#  index_payments_on_teacher_id      (teacher_id)
#
# Foreign Keys
#
#  fk_rails_30cd1cf5da  (appointment_id => appointments.id)
#

FactoryGirl.define do
  factory :payment do
    student nil
    teacher nil
    teacher_pay ""
    profit_pay ""
    total_pay ""
    paid false
    comment "MyText"
  end
end
