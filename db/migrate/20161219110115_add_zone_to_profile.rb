class AddZoneToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :zone, :string
  end
end
