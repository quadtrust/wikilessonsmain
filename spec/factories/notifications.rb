FactoryGirl.define do
  factory :notification do
    text "MyText"
    link "MyString"
    user nil
  end
end
