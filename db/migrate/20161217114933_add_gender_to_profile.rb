class AddGenderToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :gender, :integer, default: 0
  end
end
