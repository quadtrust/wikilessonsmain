
Vue.component('book-class', {
    template: '#booking-template',
    props: ['teacherid'],
    data: function () {
        return{
            message: 'Book me!!',
            show_modal: false,
            topic: '',
            description: '',
            class_booked: false,
            username: '',
            showuser: false,
            appointment_date: '',
            payment: false,
            class_valid: false,
            completed: 0
        }
    },
    methods: {
        submitAppointment: function () {
            var time = $('#datetimepicker').val();
            console.log( this.topic + this.description + " " + time);

            var that = this;
            $.ajax({
                type: 'POST',
                url: '/profiles/'+ this.teacherid +'/appointments/',
                data: {
                    appointment: {
                        appointment_date: time,
                        topic: this.topic,
                        description: this.description,
                        class_hours: this.class_hours
                    }

                },
                success: function (data) {
                    that.class_booked = true;
                    that.show_modal = false;
                    that.class_valid = false;
                    that.showuser = true;

                }
            })
        }
    },
    computed: {
      booked: function () {
          if(!this.class_valid && this.showuser && this.class_booked){
              return true;
          }
      }
    },
    mounted: function () {
        var date;
        $('#datetimepicker').datetimepicker({
            onChangeDateTime: function (currentDateTime) {
                date = currentDateTime
            }
        });
        console.log(date);
        var that = this;
        $.ajax({
            type: 'GET',
            url: '/classinfo/' + this.teacherid,
            success: function (data) {
                console.log(JSON.stringify(data));
                that.username = data.username;
                that.showuser = data.showuser;
                that.class_booked = data.class_booked;
                that.appointment_date = data.appointment_date;
                that.payment = data.payment;
                that.class_valid = data.class_valid;
                that.completed = data.completed;
            }
        })
    }
});


new Vue({
    el: '#mainapp',
    data: {
        message: 'This is a cool message'
    }
});


