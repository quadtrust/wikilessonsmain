# == Schema Information
#
# Table name: appointments
#
#  id                 :integer          not null, primary key
#  description        :text(65535)
#  accepted           :boolean          default(FALSE)
#  completed          :integer          default(0)
#  student_id         :integer
#  teacher_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  topic              :string(255)
#  class_valid        :boolean          default(FALSE)
#  appointment_date   :datetime
#  class_hours        :integer
#  job_id             :integer
#  class_status       :integer          default(0)
#  started_by_student :boolean          default(FALSE)
#  started_by_teacher :boolean          default(FALSE)
#  started_at         :datetime
#  ended_at           :datetime
#
# Indexes
#
#  index_appointments_on_student_id  (student_id)
#  index_appointments_on_teacher_id  (teacher_id)
#

class Appointment < ActiveRecord::Base
  belongs_to :student, class_name: 'User'
  belongs_to :teacher, class_name:  'User'
  has_one :payment, dependent: :destroy
  has_one :refund, dependent: :destroy
  has_many :testimonials, dependent: :destroy
  has_many :earnings

  CLASS_STATUS = {
      0 => 'Not Started',
      1 => 'Started',
      2 => 'In Progress',
      3 => 'Ended'
  }

  CLASS_STATUS_GET = {
      not_started: 0,
      started: 1,
      in_progress: 2,
      ended: 3
  }

  validates_presence_of :description, :topic

  scope :accepted_classes, -> { where(accepted: true)}

  after_create :update_payment

  def update_payment
    total_pay = self.teacher.profile.rate.to_f * self.class_hours
    profit_pay = total_pay.to_f * (12.0 / 100)
    teacher_pay = total_pay - profit_pay
    Payment.create!(
        student_id: self.student_id,
        teacher_id: self.teacher_id,
        comment: "Class requested for: #{self.appointment_date.strftime('%A, %d %b %Y %l:%M %p')}",
        appointment_id: self.id,
        total_pay: total_pay,
        teacher_pay: teacher_pay,
        profit_pay: profit_pay
    )
  end

  def set_job_for_start
    # job = self.delay(run_at: self.appointment_date).make_active
    job = self.delay(run_at: Time.now + 10.seconds).make_active
    self.update(job_id: job.id)
  end

  def set_job_for_end
    # job = self.delay(run_at: self.class_hours.hours).make_inactive
    job = self.delay(run_at: Time.now + 10.seconds).make_inactive
    Earning.create(user: self.teacher, amount: self.payment.teacher_pay)
    self.update(job_id: job.id)
  end

  def testimonial_given?(user)
    if self.testimonials.where(user: user).count > 0
      true
    else
      false
    end
  end

  private

  def make_active
    self.update(class_status: 1, job_id: nil)
  end

  def make_inactive
    self.update(completed: 1, class_status: 3, ended_at: Time.now, job_id: nil)
  end

end
