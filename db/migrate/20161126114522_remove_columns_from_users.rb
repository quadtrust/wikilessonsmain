class RemoveColumnsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :address, :text
    remove_column :users, :avatar, :string
    remove_column :users, :phone1, :string
    remove_column :users, :phone2, :string
    remove_column :users, :about, :text
    remove_column :users, :custom_video, :string
  end
end
