//= require myprofile(teacher)
//= require rate_modal
var myLatlng;
var map;
var marker;

$('.more-option').on('click',function(){
    $('.div-with-more-subject').hide();
    $('.div-without-more-subject').show();
});

function initialize() {
    myLatlng = new google.maps.LatLng(51.5, -0.12);

    var mapOptions = {
        zoom: 13,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: true,
        draggable: true
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);


    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Marker'
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
