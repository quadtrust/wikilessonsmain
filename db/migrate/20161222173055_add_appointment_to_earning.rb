class AddAppointmentToEarning < ActiveRecord::Migration
  def change
    add_reference :earnings, :appointment, index: true, foreign_key: true
  end
end
