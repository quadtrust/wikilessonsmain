class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :student, index: true
      t.references :teacher, index: true
      t.decimal :teacher_pay, precision: 16, scale: 2
      t.decimal :profit_pay, precision: 16, scale: 2
      t.decimal :total_pay, precision: 16, scale: 2
      t.boolean :paid, default: false
      t.text :comment

      t.timestamps null: false
    end
  end
end
