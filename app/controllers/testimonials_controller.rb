class TestimonialsController < ApplicationController

  def create
    @testimonial = Testimonial.new(testimonials_params)
    @testimonial.user = current_user
    @testimonial.appointment_id = params[:appointment_id]
    if current_user.has_role? :teacher
      @testimonial.to_user_id = @testimonial.appointment.student_id
    else
      @testimonial.to_user_id = @testimonial.appointment.teacher_id
    end
    if @testimonial.save
      #TODO Implement notification here
      flash[:notice] = 'Your testimonial was sent'
    else
      flash[:error] = 'There was some error, please try again'
    end
    redirect_to :back
  end

  private

  def testimonials_params
    params.require(:testimonial).permit(:description, :rating)
  end

end
