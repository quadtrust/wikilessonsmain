class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :brand
      t.string :country
      t.string :month
      t.string :year
      t.string :last_digits
      t.boolean :default

      t.timestamps null: false
    end
  end
end
