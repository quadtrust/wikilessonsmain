class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.text :text
      t.string :link
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
