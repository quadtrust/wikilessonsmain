class CreateRefunds < ActiveRecord::Migration
  def change
    create_table :refunds do |t|
      t.boolean :status
      t.integer :amount
      t.text :comment
      t.belongs_to :appointment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
